
def menu(request):
	if request.user.is_authenticated():
		ctx = {'profile':request.user}
		return ctx
	else:
		return {'profile': ''}