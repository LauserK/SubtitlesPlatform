//var BASE_URL = "http://sub-lauserk.herokuapp.com/";
var BASE_URL = "http://localhost:8000/";
angular.module("FinalApp")
.controller("MainController", function($scope, $http, $resource, LastSubsSeries, LastSubsMovies){
	$scope.series = LastSubsSeries.get();
	$scope.movies = LastSubsMovies.get();
})
.controller("SerieController", function($scope,$resource, SeriesResource, SubtitlesResource, $routeParams, $location){
	$scope.serie = SeriesResource.get({id: $routeParams.id}, function(data){
		var seasons = [];
		console.log(data);
		for (var i = 1; i <= data.seasons; i++) {
			seasons.push(i);
		}
		$scope.seasons = seasons;
	});
	subs = $resource(BASE_URL+"subs/:id/", {id: "@id"});
	$scope.subtitles = subs.get({id: $routeParams.id});
})
.controller("MovieController", function($scope,$resource, MoviesResource, $routeParams, $location){
	$scope.movie = MoviesResource.get({id: $routeParams.id});	
	subs = $resource(BASE_URL+"subs/movie/:id/", {id: "@id"});
	$scope.subtitles = subs.get({id: $routeParams.id});
})
.controller("AddSerieController", function($scope, $location, LxNotificationService){	
	$scope.saveSub = function(){
		$.ajaxSetup({
			beforeSend: function(xhr, settings) {
				if(settings.type == "POST"){
					xhr.setRequestHeader("X-CSRFToken", $('[name="csrfmiddlewaretoken"]').val());
				}
			}
		});
		var title = $('#title').val(),
		year = $('#year').val(),
		seasons = $('#seasons').val(),		
		description = $('#description').val()
		cover = $('#cover').val();

		if (name != '' && year != '' && seasons != '' && cover != ''){
			$.post(BASE_URL+'add/serie/',{title: title, year: year,seasons: seasons, description: description, cover: cover
			}, serieSend);
		}		
	};	

	function serieSend(data){
	    LxNotificationService.confirm('Serie agregada!', '', {ok:"Aceptar" },function(answer)
	    {	    	
	        $location.path("#/");
	    });
	}
})
.controller("SubAddSerieController", function($scope, $http, $resource, $location, $routeParams, LxNotificationService, SeriesResource){
	$scope.serie = SeriesResource.get({id: $routeParams.id});
	$scope.saveSub = function(){
		$.ajaxSetup({
			beforeSend: function(xhr, settings) {
				if(settings.type == "POST"){
					xhr.setRequestHeader("X-CSRFToken", $('[name="csrfmiddlewaretoken"]').val());
				}
			}
		});
		var name = $('#chapter').val(),
		season = $('#number_of_season').val(),
		chapter = $('#number_of_chapter').val(),
		file = $('#file_srt').val(),
		version_of_video = $('#version_of_video').val()
		comments = $('#comments').val();

		if (name != '' && season != '' && chapter != '' && file != ''){
			$.post(BASE_URL+'add-sub/serie/', {serie_id: $routeParams.id, name: name,season: season,chapter: chapter,file: file,comments: comments,version: version_of_video,
			}, subSend);
		}		
	};	

	function subSend(data){
	    LxNotificationService.confirm('Subtitulo agregado!', 'Tu subtitulo a sido agregado exitosamente.\nGracias por ser parte de nuestra comunidad.', {ok:"Aceptar" },function(answer)
	    {	 
	    	console.log();
	        $location.path("#/serie/"+$routeParams.id);
	    });
	}
})
.controller("SubAddMovieController", function($scope, $http, $resource, $location, $routeParams, LxNotificationService, MoviesResource){
	$scope.movie = MoviesResource.get({id: $routeParams.id});
	$scope.saveSub = function(){
		$.ajaxSetup({
			beforeSend: function(xhr, settings) {
				if(settings.type == "POST"){
					xhr.setRequestHeader("X-CSRFToken", $('[name="csrfmiddlewaretoken"]').val());
				}
			}
		});
		var file = $('#file_srt').val(),
		version_of_video = $('#version_of_video').val()
		comments = $('#comments').val();

		if (file != '' && version_of_video != ''){
			$.post(BASE_URL+'add-sub/movie/', {movie_id: $routeParams.id,file: file,comments: comments,version: version_of_video,
			}, subSend);
		}		
	};	

	function subSend(data){
	    LxNotificationService.confirm('Subtitulo agregado!', 'Tu subtitulo a sido agregado exitosamente.\nGracias por ser parte de nuestra comunidad.', {ok:"Aceptar" },function(answer)
	    {	 
	    	console.log();
	        $location.path("#/movie/"+$scope.movie.id);
	    });
	}
})
.controller("ReportSubController", function($scope, $http, $routeParams, $location, LxNotificationService){
	$scope.savePost = function(){
		$.ajaxSetup({
			beforeSend: function(xhr, settings) {
				if(settings.type == "POST"){
					xhr.setRequestHeader("X-CSRFToken", $('[name="csrfmiddlewaretoken"]').val());
				}
			}
		});
		var input = $('#reason_input');
		if (input.val() != '' && input.val().length >= 6 && input.val().length <=140){
			$.post(BASE_URL+'send-report/', { reason: input.val(), subtitle_id: $routeParams.id}, report_send);
		}
	}
	function report_send(data){
	    LxNotificationService.confirm('Reporte Enviado', 'Gracias por enviar el reporte. Nuestro equipo revisará el subtitulo.', {ok:"Gracias!" },function(answer)
	    {
	        //console.log(answer);
	        $location.path("/");
	    });
	}
})
.controller("ReportMovieSubController", function($scope, $http, $routeParams, $location, LxNotificationService){
	$scope.savePost = function(){
		$.ajaxSetup({
			beforeSend: function(xhr, settings) {
				if(settings.type == "POST"){
					xhr.setRequestHeader("X-CSRFToken", $('[name="csrfmiddlewaretoken"]').val());
				}
			}
		});
		var input = $('#reason_input');
		if (input.val() != '' && input.val().length >= 6 && input.val().length <=140){
			$.post(BASE_URL+'send-report-movie/', { reason: input.val(), subtitle_id: $routeParams.id}, report_send);
		}
	}
	function report_send(data){
	    LxNotificationService.confirm('Reporte Enviado', 'Gracias por enviar el reporte. Nuestro equipo revisará el subtitulo.', {ok:"Gracias!" },function(answer)
	    {
	        //console.log(answer);
	        $location.path("/");
	    });
	}
});