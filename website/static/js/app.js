//var BASE_URL = "http://sub-lauserk.herokuapp.com/";
var BASE_URL = "http://localhost:8000/";
angular.module("FinalApp",["lumx","ngRoute", "ngResource", 'angularMoment'])
.config(function($routeProvider, $httpProvider){
	$routeProvider
		// Home
		.when("/",{
			controller: "MainController",
			templateUrl: BASE_URL+"static/templates/home.html"
		})
		// Add Serie (Only Admin)
		.when("/add/serie",{
			controller: "AddSerieController",
			templateUrl: BASE_URL+"static/templates/add_serie.html"
		})
		.when("/serie", {
			controller: "MovieViewController",
			templateUrl: BASE_URL+"static/templates/series.html"
		})
		// View Serie with subtitles
		.when("/serie/:id",{
			controller: "SerieController",
			templateUrl: BASE_URL+"static/templates/serie.html"
		})
		// Add Subtitle (Serie)
		.when("/serie/:id/add",{
			controller: "SubAddSerieController",
			templateUrl: BASE_URL+"static/templates/serie_add_sub.html"
		})
		// View movie with subtitles
		.when("/movie/:id",{
			controller: "MovieController",
			templateUrl: BASE_URL+"static/templates/movie.html"
		})
		// Add Subtitle (Movie)
		.when("/movie/:id/add",{
			controller: "SubAddMovieController",
			templateUrl: BASE_URL+"static/templates/movie_add_sub.html"
		})
		// Report subtitle of serie
		.when("/report/:id",{
			controller: "ReportSubController",
			templateUrl: BASE_URL+"static/templates/report_form.html"
		})
		// Report subtitle of movie
		.when("/report/movie/:id",{
			controller: "ReportMovieSubController",
			templateUrl: BASE_URL+"static/templates/report_form.html"
		})
		// Any other link get 404
		.otherwise({
			templateUrl: BASE_URL+"static/templates/404.html"
			//redirectTo: '/'
    	});
});