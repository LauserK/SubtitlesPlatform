//var BASE_URL = "http://sub-lauserk.herokuapp.com/";
var BASE_URL = "http://localhost:8000/";
angular.module("FinalApp")
.factory("SeriesResource", function($resource){
	return Serie = $resource(BASE_URL+"api/series/:id?format=json", {id: "@id"}, {update: {method: "PUT"}});
})
.factory("MoviesResource", function($resource){
	return Movie = $resource(BASE_URL+"api/movies/:id?format=json", {id: "@id"}, {update: {method: "PUT"}});
})
.factory("LastSubsSeries", function($resource){
	return Series = $resource(BASE_URL+"last-subs/series/");
})
.factory("LastSubsMovies", function($resource){
	return Series = $resource(BASE_URL+"last-subs/movies/");
})
.factory("SubtitlesResource", function($resource){
	return Subtitles = $resource(BASE_URL+"api/subtitles/", {id: "@id"}, {update: {method: "PUT"}});
})
.factory("ReportResource", function($resource){
	return Report = $resource(BASE_URL+"api/report/");
});