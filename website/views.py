# coding=utf-8
from django.contrib.auth import logout
from django.http import HttpResponseRedirect
from django.shortcuts import render
#from django.views.decorators.csrf import csrf_exempt


def home(request):
	return render(request, 'website/home.html')

def log_out(request):
    logout(request)
    return HttpResponseRedirect("/")