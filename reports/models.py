from django.db import models
from subtitles.models import Subtitle, SubtitleMovie
from django.contrib.auth.models import User

class Report(models.Model):
	user = models.ForeignKey(User)
	subtitle = models.ForeignKey(Subtitle)
	reason = models.TextField(help_text='Please put your reason...')

	def __unicode__(self):
		return '%s reported | id: %s' % (self.subtitle, self.subtitle.pk)

class ReportMovieSub(models.Model):
	user = models.ForeignKey(User)
	subtitle = models.ForeignKey(SubtitleMovie)
	reason = models.TextField(help_text='Please put your reason...')

	def __unicode__(self):
		return '%s reported | id: %s' % (self.subtitle, self.subtitle.pk)