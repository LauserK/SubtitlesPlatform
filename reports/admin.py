from django.contrib import admin
from .models import Report, ReportMovieSub

@admin.register(Report)
class ReportAdmin(admin.ModelAdmin):
	pass

@admin.register(ReportMovieSub)
class ReportMovieSubAdmin(admin.ModelAdmin):
	pass