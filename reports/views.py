from .models import Report, ReportMovieSub
from subtitles.models import Subtitle, SubtitleMovie
from .serializers import ReportSerializer, ReportMovieSerializer
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, Http404, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from rest_framework import viewsets

class ReportViewSet(viewsets.ModelViewSet):
	allowed_methods = ('POST',)
	queryset = Report.objects.all()
	serializer_class = ReportSerializer

class ReportMovieViewSet(viewsets.ModelViewSet):
	allowed_methods = ('POST',)
	queryset = ReportMovieSub.objects.all()
	serializer_class = ReportMovieSerializer

@login_required(login_url='/login')
def CreateReport(request):
	if request.is_ajax():
		if request.POST['reason']:
			subtitle = Subtitle.objects.get(pk=request.POST['subtitle_id'])
			reason = request.POST['reason']
			if len(reason) < 5:
				pass
			report = Report(user=request.user, subtitle=subtitle ,reason=request.POST['reason'])
			report.save()

		return JsonResponse({"message": "Ok"})
	else:
		raise Http404

@login_required(login_url='/login')
def CreateMovieReport(request):
	if request.is_ajax():
		if request.POST['reason']:
			subtitle = SubtitleMovie.objects.get(pk=request.POST['subtitle_id'])
			reason = request.POST['reason']
			if len(reason) < 5:
				pass
			report = ReportMovieSub(user=request.user, subtitle=subtitle ,reason=request.POST['reason'])
			report.save()

		return JsonResponse({"message": "Ok"})
	else:
		raise Http404