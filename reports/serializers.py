from rest_framework import serializers
from .models import Report, ReportMovieSub
from django.contrib.auth.models import User
from subtitles.models import Subtitle

class ReportSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Report
		fields = ('user', 'subtitle', 'reason', )

class ReportMovieSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = ReportMovieSub
		fields = ('user', 'subtitle', 'reason', )