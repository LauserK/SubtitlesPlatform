angular.module("FinalApp")
.controller("MainController", function($scope, $http, $resource, LastSubsSeries, MoviesResource){
	$scope.series = LastSubsSeries.get();
	console.log($scope.series);
	$scope.movies = MoviesResource.query();


	$scope.opendDialog = function(dialogId)
	{
	    LxDialogService.open(dialogId);
	};

	$scope.closingDialog = function()
	{
	    LxNotificationService.info('Dialog closed!');
	};
})
.controller("SerieController", function($scope,$resource, SeriesResource, SubtitlesResource, $routeParams, $location){
	$scope.serie = SeriesResource.get({id: $routeParams.id}, function(data){
		var seasons = [];
		for (var i = 1; i <= data.get_seasons; i++) {
			seasons.push(i);
		}
		$scope.seasons = seasons;
	});
	subs = $resource("http://localhost:8000/subs/:id/", {id: "@id"});
	$scope.subtitles = subs.get({id: $routeParams.id});
})
.controller("MovieController", function($scope,$resource, MoviesResource, $routeParams, $location){
	$scope.movie = MoviesResource.get({id: $routeParams.id});
	subs = $resource("http://localhost:8000/subs/movie/:id/", {id: "@id"});
	$scope.subtitles = subs.get({id: $routeParams.id});
})
.controller("ReportSubController", function($scope, $http, $routeParams, $location, LxNotificationService){
	$scope.savePost = function(){
		$.ajaxSetup({
			beforeSend: function(xhr, settings) {
				if(settings.type == "POST"){
					xhr.setRequestHeader("X-CSRFToken", $('[name="csrfmiddlewaretoken"]').val());
				}
			}
		});
		var input = $('#reason_input');
		if (input.val() != '' && input.val().length >= 6 && input.val().length <=140){
			$.post('/send-report/', { reason: input.val(), subtitle_id: $routeParams.id}, report_send);
		}
	}
	function report_send(data){
	    LxNotificationService.confirm('Reporte Enviado', 'Gracias por enviar el reporte. Nuestro equipo revisará el subtitulo.', {ok:"Gracias!" },function(answer)
	    {
	        //console.log(answer);
	        $location.path("/");
	    });
	}
})
.controller("ReportMovieSubController", function($scope, $http, $routeParams, $location, LxNotificationService){
	$scope.savePost = function(){
		$.ajaxSetup({
			beforeSend: function(xhr, settings) {
				if(settings.type == "POST"){
					xhr.setRequestHeader("X-CSRFToken", $('[name="csrfmiddlewaretoken"]').val());
				}
			}
		});
		var input = $('#reason_input');
		if (input.val() != '' && input.val().length >= 6 && input.val().length <=140){
			$.post('/send-report-movie/', { reason: input.val(), subtitle_id: $routeParams.id}, report_send);
		}
	}
	function report_send(data){
	    LxNotificationService.confirm('Reporte Enviado', 'Gracias por enviar el reporte. Nuestro equipo revisará el subtitulo.', {ok:"Gracias!" },function(answer)
	    {
	        //console.log(answer);
	        $location.path("/");
	    });
	}
});