angular.module("FinalApp")
.factory("SeriesResource", function($resource){
	return Serie = $resource("http://localhost:8000/api/series/:id?format=json", {id: "@id"}, {update: {method: "PUT"}});
})
.factory("LastSubsSeries", function($resource){
	return Series = $resource("http://localhost:8000/last-subs/series/");
})
.factory("LastSubsMovies", function($resource){
	return Series = $resource("http://localhost:8000/last-subs/movies/");
})
.factory("MoviesResource", function($resource){
	return Movie = $resource("http://localhost:8000/api/movies/:id?format=json", {id: "@id"}, {update: {method: "PUT"}});
})
.factory("SubtitlesResource", function($resource){
	return Subtitles = $resource("http://localhost:8000/api/subtitles/", {id: "@id"}, {update: {method: "PUT"}});
})
.factory("ReportResource", function($resource){
	return Report = $resource("http://localhost:8000/api/report/");
});