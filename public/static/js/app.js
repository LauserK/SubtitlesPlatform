angular.module("FinalApp",["lumx","ngRoute", "ngResource", 'angularMoment'])
.config(function($routeProvider, $httpProvider){
	$routeProvider
		.when("/",{
			controller: "MainController",
			templateUrl: "http://localhost:8000/static/templates/home.html"
		})
		.when("/serie/:id",{
			controller: "SerieController",
			templateUrl: "http://localhost:8000/static/templates/serie.html"
		})
		.when("/movie/:id",{
			controller: "MovieController",
			templateUrl: "http://localhost:8000/static/templates/movie.html"
		})
		.when("/report/:id",{
			controller: "ReportSubController",
			templateUrl: "http://localhost:8000/static/templates/report_form.html"
		})
		.when("/report/movie/:id",{
			controller: "ReportMovieSubController",
			templateUrl: "http://localhost:8000/static/templates/report_form.html"
		})		
});