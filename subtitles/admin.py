from django.contrib import admin
from .models import Serie, Subtitle, Movie, SubtitleMovie

@admin.register(Serie)
class SerieAdmin(admin.ModelAdmin):
	exclude = ('slug',)

@admin.register(Movie)
class MovieAdmin(admin.ModelAdmin):
	exclude = ('slug',)

@admin.register(Subtitle)
class SubtitleAdmin(admin.ModelAdmin):
	exclude = ('date',)

@admin.register(SubtitleMovie)
class SubtitleMovieAdmin(admin.ModelAdmin):
	exclude = ('date',)