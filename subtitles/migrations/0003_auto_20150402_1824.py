# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('subtitles', '0002_auto_20150401_1812'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='subtitle',
            name='language',
        ),
        migrations.RemoveField(
            model_name='subtitlemovie',
            name='language',
        ),
    ]
