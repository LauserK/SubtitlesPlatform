# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Movie',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=140)),
                ('year', models.IntegerField(max_length=4)),
                ('slug', models.SlugField(unique=True, blank=True)),
                ('description', models.TextField(default=b'Not Description', max_length=320)),
                ('cover', models.CharField(max_length=140)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Serie',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=140)),
                ('slug', models.SlugField(unique=True, blank=True)),
                ('year', models.IntegerField(max_length=4)),
                ('description', models.TextField(default=b'Not Description', max_length=320)),
                ('seasons', models.IntegerField(default=2)),
                ('cover', models.CharField(max_length=140)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Subtitle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('chapter_name', models.CharField(default=b'', max_length=140)),
                ('number_of_season', models.IntegerField()),
                ('number_of_chapter', models.IntegerField()),
                ('language', models.CharField(default=b'esp', max_length=10, choices=[(b'esp', b'Spanish'), (b'eng', b'English')])),
                ('file_srt', models.FileField(help_text=b'Select the subtitle file (.srt)', upload_to=b'subtitles/series')),
                ('version_of_video', models.CharField(help_text=b'Put the version of the video', max_length=140)),
                ('comments', models.TextField(help_text=b"WARNING: don't put download links or you will be banned", blank=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('serie', models.ForeignKey(to='subtitles.Serie')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SubtitleMovie',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('language', models.CharField(default=b'esp', max_length=10, choices=[(b'esp', b'Spanish'), (b'eng', b'English')])),
                ('file_srt', models.FileField(help_text=b'Select the subtitle file (.srt)', upload_to=b'subtitles/movies')),
                ('version_of_video', models.CharField(help_text=b'Put the version of the video', max_length=140)),
                ('comments', models.TextField(help_text=b"WARNING: don't put download links or you will be banned", blank=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('movie', models.ForeignKey(to='subtitles.Movie')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
