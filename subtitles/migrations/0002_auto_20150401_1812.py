# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('subtitles', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subtitle',
            name='file_srt',
            field=models.CharField(help_text=b'Link to the MEGA file url', max_length=200),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='subtitlemovie',
            name='file_srt',
            field=models.CharField(help_text=b'Link to the MEGA file url', max_length=200),
            preserve_default=True,
        ),
    ]
