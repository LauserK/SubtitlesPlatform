from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.template.defaultfilters import slugify
from cloudinary.models import CloudinaryField

class Serie(models.Model):
	title = models.CharField(max_length=140)
	slug = models.SlugField(unique=True, blank=True)
	year = models.IntegerField(max_length=4)
	description = models.TextField(default="Not Description", max_length=320)
	seasons = models.IntegerField(default=2)
	cover = models.CharField(max_length=140)	

	def __unicode__(self):
		return self.title

	# Auto Slug
	def save(self, *args, **kwargs):
		self.slug = slugify(self.title)
		super(Serie, self).save(*args, **kwargs)

class Movie(models.Model):
	title = models.CharField(max_length=140)
	year = models.IntegerField(max_length=4)
	slug = models.SlugField(unique=True, blank=True)
	description = models.TextField(default="Not Description", max_length=320)
	cover = models.CharField(max_length=140)

	def __unicode__(self):
		return self.title

	# Auto Slug
	def save(self, *args, **kwargs):
		self.slug = slugify(self.title)
		super(Movie, self).save(*args, **kwargs)

class Subtitle(models.Model):
	serie = models.ForeignKey(Serie)
	user = models.ForeignKey(User)
	chapter_name = models.CharField(max_length=140, default="")
	number_of_season = models.IntegerField()
	number_of_chapter = models.IntegerField()
	file_srt = models.CharField(max_length=200, help_text='Link to the MEGA file url')
	version_of_video = models.CharField(max_length=140, help_text='Put the version of the video')
	comments = models.TextField(blank=True,help_text="WARNING: don't put download links or you will be banned")
	date = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return "%sx%s | %s" % (self.number_of_season, self.number_of_chapter, self.serie.title)

	def get_season(self):
		return self.number_of_season

	def get_chapter(self):
		return self.number_of_season

	def get_sub_url(self):
		return "%s/%s" % (settings.MEDIA_URL, self.file_srt)

	def get_serie(self):
		return self.serie.id

class SubtitleMovie(models.Model):
	movie = models.ForeignKey(Movie)
	user = models.ForeignKey(User)
	file_srt = models.CharField(max_length=200, help_text='Link to the MEGA file url')
	version_of_video = models.CharField(max_length=140, help_text='Put the version of the video')
	comments = models.TextField(blank=True,help_text="WARNING: don't put download links or you will be banned")
	date = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return "Subtitle: %s" % self.movie

	def get_sub_url(self):
		return "%s/%s" % (settings.MEDIA_URL, self.file_srt)

	def get_movie(self):
		return self.movie.id