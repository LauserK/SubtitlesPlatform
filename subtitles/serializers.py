from rest_framework import serializers
from .models import Serie, Subtitle, Movie, SubtitleMovie
from django.contrib.auth.models import User

class SerieSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Serie
		fields = ('url','id', 'title','slug','seasons','cover', 'description', 'year')

class SubtitlesSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Subtitle
		fields = ('url', 'id', 'get_serie','user', 'chapter_name', 'number_of_season', 'get_chapter', 'version_of_video', 'get_sub_url',)

class SubtitleMovieSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = SubtitleMovie
		fields = ('url', 'id', 'get_movie', 'user', 'version_of_video', 'get_sub_url')

class MovieSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Movie
		fields = ('url', 'id', 'slug', 'slug', 'title', 'description', 'year', 'cover')

class UserSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = User
		fields = ('username','email')
