#from django.views.decorators.csrf import ensure_csrf_cookie
from .models import Serie, Subtitle, Movie, SubtitleMovie
from .serializers import SubtitlesSerializer, UserSerializer, SerieSerializer, MovieSerializer, SubtitleMovieSerializer
from django.conf import settings
from django.contrib.auth.models import User
from django.http import HttpResponse, Http404
from django.http import JsonResponse
from django.shortcuts import render
from rest_framework import viewsets
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
import json

class SerieViewSet(viewsets.ModelViewSet):
	allowed_methods = ('GET',)
	queryset = Serie.objects.all()
	serializer_class = SerieSerializer

class MovieViewSet(viewsets.ModelViewSet):
	allowed_methods = ('GET',)
	queryset = Movie.objects.all()
	serializer_class = MovieSerializer

class SubtitleViewSet(viewsets.ModelViewSet):
	allowed_methods = ('GET','POST')
	queryset = Subtitle.objects.all()
	serializer_class = SubtitlesSerializer

class SubtitleMovieViewSet(viewsets.ModelViewSet):
	allowed_methods = ('GET','POST')
	queryset = SubtitleMovie.objects.all()
	serializer_class = SubtitleMovieSerializer

class UserViewSet(viewsets.ModelViewSet):
	allowed_methods = ('GET',)
	queryset = User.objects.all()
	serializer_class = UserSerializer


# API MANUAL

def load_subtitles(request, serie_id):
	serie = Serie.objects.get(pk=serie_id)
	subs = Subtitle.objects.all().filter(serie=serie).order_by('-date').order_by('number_of_chapter')
	data = list()

	for sub in subs:
		data.append({
			'id': sub.pk, 
			'user': sub.user.username,
			'chapter_name': sub.chapter_name,
			'season': sub.number_of_season,
			'chapter': sub.number_of_chapter,			
			'file': sub.file_srt,
			'version': sub.version_of_video,
			'comment': sub.comments,
		})

	return JsonResponse({"subtitles": data})

def load_subtitles_movies(request, movie_id):
	movie = Movie.objects.get(pk=movie_id)
	subs = SubtitleMovie.objects.all().filter(movie=movie).order_by('-date')
	data = list()

	for sub in subs:
		data.append({
			'id': sub.pk, 
			'user': sub.user.username,						
			'file': sub.file_srt,
			'version': sub.version_of_video,
			'comment': sub.comments,
		})

	return JsonResponse({"subtitles": data})

def GetLastSeriesSubtitles(request):
	subtitles = Subtitle.objects.all().order_by("-date")[0:5]

	data = list()

	for sub in subtitles:
		data.append({
			'id': sub.serie.pk,
			'user': sub.user.username,			
			'title': sub.serie.title,
			'cover': sub.serie.cover,
			'season': sub.number_of_season,
			'cap': sub.number_of_chapter,
			'date': sub.date,
			'version': sub.version_of_video
		})

	return JsonResponse({"subtitles": data})

def GetLastMoviesSubtitles(request):
	subtitles = SubtitleMovie.objects.all().order_by("-date")[0:5]

	data = list()

	for sub in subtitles:
		data.append({
			'id': sub.movie.pk,
			'user': sub.user.username,			
			'title': sub.movie.title,
			'year': sub.movie.year,			
			'cover': sub.movie.cover,
			'date': sub.date,
			'version': sub.version_of_video		
		})

	return JsonResponse({"subtitles": data})

@login_required(login_url='/login')
def AddSubSerie(request):
	if request.is_ajax():
		if request.POST:
			serie = Serie.objects.get(pk=request.POST['serie_id'])
			name = request.POST['name']
			season = request.POST['season']
			chapter = request.POST['chapter']
			srt = request.POST['file']
			comments = request.POST['comments']
			version = request.POST['version']
			
			subtitle = Subtitle(serie=serie, user=request.user, chapter_name=name, number_of_season=season, number_of_chapter=chapter, file_srt=srt, version_of_video=version, comments=comments)				
			subtitle.save()

		return JsonResponse({"message": "Ok"})
	else:
		raise Http404

@login_required(login_url='/login')
def AddSubMovie(request):
	if request.is_ajax():
		if request.POST:
			movie = Movie.objects.get(pk=request.POST['movie_id'])			
			srt = request.POST['file']
			comments = request.POST['comments']
			version = request.POST['version']
			
			subtitle = SubtitleMovie(movie=movie, user=request.user,file_srt=srt, version_of_video=version, comments=comments)				
			subtitle.save()

		return JsonResponse({"message": "Ok"})
	else:
		raise Http404

#@staff_member_required
def AddSerie(rquest):
	if request.is_ajax():
		if request.POST:
			title = request.POST['title']
			year = request.POST['year']
			seasons = request.POST['seasons']
			description = request.POST['description']
			cover = request.POST['cover']
			
			serie = Serie(title=title, year=year, description=description, seasons=seasons, cover=cover)				
			serie.save()

		return JsonResponse({"message": "Ok"})
	else:
		raise Http404	