from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework import routers
from subtitles.views import SubtitleViewSet, UserViewSet, SerieViewSet, MovieViewSet, SubtitleMovieViewSet
from reports.views import ReportViewSet, ReportMovieViewSet
from django.conf import settings

router = routers.DefaultRouter()
router.register(r'subtitles', SubtitleViewSet)
router.register(r'subtitles-movies', SubtitleMovieViewSet)
router.register(r'series', SerieViewSet)
router.register(r'movies', MovieViewSet)
router.register(r'user', UserViewSet)
router.register(r'report', ReportViewSet)
router.register(r'report-movie', ReportMovieViewSet)

urlpatterns = patterns('',
    # Home:
    url(r'^$', "website.views.home", name='home'),
    
    # Auth and Registration views
    #url(r'^login/$', "website.views.login_user", name='login'),

    # GET ALL SUBS OF SERIE
    url(r'^subs/(?P<serie_id>[\w\d\-]+)/$', 'subtitles.views.load_subtitles', name='subs'),
    url(r'^subs/movie/(?P<movie_id>[\w\d\-]+)/$', 'subtitles.views.load_subtitles_movies', name='subs_movies'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/', include(router.urls)),
	url(r'^media/(?P<path>.*)$','django.views.static.serve', {'document_root':settings.MEDIA_ROOT,}),

    # Last subtitles of movies and series
    url(r'^last-subs/series/$', "subtitles.views.GetLastSeriesSubtitles", name='lastSubsSeries'),
    url(r'^last-subs/movies/$', "subtitles.views.GetLastMoviesSubtitles", name='lastSubsMovies'),
	# Send Report [POST]:
    url(r'^send-report/$', "reports.views.CreateReport", name='send_report'),
    url(r'^send-report-movie/$', "reports.views.CreateMovieReport", name='send_report'),

    # Add Serie
    url(r'^add/serie/$', "subtitles.views.AddSerie", name='AddSerie'),
    # Add subtitle
    url(r'^add-sub/serie/$', "subtitles.views.AddSubSerie", name='AddSubSerie'),
    url(r'^add-sub/movie/$', "subtitles.views.AddSubMovie", name='AddSubMovie'),
    # Python Social Auth
    url(r'^logout/$', 'website.views.log_out', name='log_out'),
    url(r'', include('social_auth.urls')),
)
